#!/bin/bash

$ docker run --rm -it --link web_service_container wernight/ngrok ngrok http web_service_container:80
