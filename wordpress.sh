#!/bin/bash

echo "+ Removing actual stack of Wordpress"
docker stack rm wordpress
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/wordpress/wordpress.yml
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/wordpress/wordpress.yml -P /home/david/docker-stacks/wordpress/
echo "+ Deploying new Wordpress stack"
docker stack deploy -c /home/david/docker-stacks/wordpress/wordpress.yml wordpress
