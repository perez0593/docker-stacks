#!/bin/bash

echo "+ Removing actual stack of MailServer"
docker stack rm mailserver
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/mailserver/mailserver.yml
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/mailserver/mailserver.yml -P /home/david/docker-stacks/mailserver/
mv /home/david/docker-stacks/mailserver/mailserver.yml /home/david/docker-stacks/mailserver/docker-compose.yml 
echo "+ Waiting 10s for the correct deployment of the MailServer..."
sleep 10s
echo "+ Deploying new MailServer stack"
docker stack deploy -c /home/david/docker-stacks/mailserver/docker-compose.yml mailserver
