#!/bin/bash

echo "+ Removing actual stack of SwarmPit"
docker stack rm swarmpit
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/swarmpit/swarmpit.yml 
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/swarmpit/swarmpit.yml -P /home/david/docker-stacks/swarmpit/
echo "+ Deploying new SwarmPit stack"
docker stack deploy -c /home/david/docker-stacks/swarmpit/swarmpit.yml swarmpit
