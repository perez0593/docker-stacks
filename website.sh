#!/bin/bash

echo "+ Removing actual stack of Website"
docker stack rm website
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/website/website.yml 
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/website/website.yml -P /home/david/docker-stacks/website/
echo "+ Downloading latest website files..."
rm -r /opt/sysadmin/volumes/website/*
mkdir /opt/sysadmin/volumes/website/html
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/website/nginx-config.conf -P /opt/sysadmin/volumes/website/
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/website/html/index.html -P /opt/sysadmin/volumes/website/html/
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/website/html/functions.js -P /opt/sysadmin/volumes/website/html/
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/website/html/styles.css -P /opt/sysadmin/volumes/website/html/
echo "Done.."
echo "+ Waiting 10s for new deployment..."
sleep 10s
echo "+ Deploying new Website stack"
docker stack deploy -c /home/david/docker-stacks/website/website.yml website
