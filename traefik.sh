#!/bin/bash

echo "+ Removing actual stack of Traefik"
docker stack rm traefik
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/traefik/traefik.yml 
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/traefik/traefik.yml -P /home/david/docker-stacks/traefik/
echo "+ Deploying new Traefik stack"
docker stack deploy -c /home/david/docker-stacks/traefik/traefik.yml traefik
