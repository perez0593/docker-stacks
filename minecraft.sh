#!/bin/bash

echo "+ Removing actual stack of Minecraft"
docker stack rm minecraft-server
echo "+ Removing actual YAML config file"
rm /home/david/docker-stacks/minecraft/minecraft.yml
echo "+ Downloading new YAML config file"
wget https://gitlab.com/perez0593/docker-stacks/-/raw/master/minecraft/minecraft.yml -P /home/david/docker-stacks/minecraft/
echo "+ Deploying new SwarmPit stack"
docker stack deploy -c /home/david/docker-stacks/minecraft/minecraft.yml minecraft-server
