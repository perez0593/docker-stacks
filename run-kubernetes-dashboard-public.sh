#!/bin/bash

echo "+ Starting kubernetes dashboard"
echo "+ Access from here domain:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/workloads?namespace=default"
kubectl proxy --address='0.0.0.0' --disable-filter=true
